# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile



# for DexGuard only
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule
 #### -- OkHttp --

-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
-dontwarn org.conscrypt.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

-dontwarn com.google.errorprone.annotations.**

 -dontwarn com.squareup.okhttp.**

 -dontwarn  com.squareup.retrofit2.**
 #### -- Apache Commons --

 -dontwarn org.apache.commons.logging.**

# Make Crashlytics reports more informative
-keepattributes SourceFile,LineNumberTable

# Allow obfuscation of android.support.v7.internal.view.menu.**
# to avoid problem on Samsung 4.2.2 devices with appcompat v21
# see https://code.google.com/p/android/issues/detail?id=78377
-keep class !android.support.v7.internal.view.menu.**,android.support.** {*;}
# Don't break support libraries
-keep interface android.support.** { *; }
-dontwarn android.support.**
# Used to animate values of an Observable with an ObjectAnimator
# (since it calls setters by name using reflection)
# -keepnames class * extends android.databinding.BaseObservable { *; }

# From https://github.com/google/gson/blob/master/examples/android-proguard-example/proguard.cfg
-keepattributes Signature
-keep class sun.misc.Unsafe { *; }


# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# GSON rules
# Don't obfuscate instance field names for GSON
-keepnames class com.online.job.test.pojo.** { *; }
-keepnames class com.online.job.test.activities.** { *; }
-keepnames class com.online.job.test.utils.TaskWorker.** { *; }

# From https://github.com/google/gson/blob/master/examples/android-proguard-example/proguard.cfg
-keepattributes Signature
-keep class sun.misc.Unsafe { *; }
-keep class App
## Retrolambda specific rules ##
# as per official recommendation: https://github.com/evant/gradle-retrolambda#proguard
-dontwarn java.lang.invoke.*

## Retrofit + Retrolambda ##
-dontwarn retrofit2.Platform$Java8

## OkHttp ##
-dontwarn okio.**
-dontwarn javax.annotation.**
## RxJava ##
-dontwarn sun.misc.**

-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}
