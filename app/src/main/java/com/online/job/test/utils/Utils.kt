package com.online.job.test.utils

import android.content.Context
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import java.io.BufferedReader
import java.lang.Long
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


// Extension function to show toast message
fun  toastLong(context : Context?, message: String) {
    AndroidSchedulers.mainThread().scheduleDirect {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}
// Extension function to show toast message
fun  toast(context : Context?, message: String) {
    AndroidSchedulers.mainThread().scheduleDirect{
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}

  fun getDateTime(timeStampStr : String): String {
      return try {
        val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
        val netDate = Date(Long.parseLong(timeStampStr + "000"))
         sdf.format(netDate)
    } catch (ignored: Exception) {
          "-"
    }

}

fun getDate(timeStampStr : String): String {
    return try {
        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val netDate = Date(Long.parseLong(timeStampStr + "000"))
        sdf.format(netDate)
    } catch (ignored: Exception) {
        "-"
    }
}
fun getTime(timeStampStr : String): String {
    return try {
        val sdf = SimpleDateFormat("HH:mm", Locale.getDefault())
        val netDate = Date(Long.parseLong(timeStampStr + "000"))
        sdf.format(netDate)
    } catch (ignored: Exception) {
        "-"
    }
}