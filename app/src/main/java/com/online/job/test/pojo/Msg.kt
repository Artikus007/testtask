package com.online.job.test.pojo

import com.google.gson.annotations.Expose

data class Msg (
    @Expose
    var message : String,
    @Expose
    var fields :  Fields? = null
)