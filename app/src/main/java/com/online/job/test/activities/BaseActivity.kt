package com.online.job.test.activities

import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import com.online.job.test.data.UserPreferences
import com.online.job.test.dataloaders.DBLoader
import com.online.job.test.dataloaders.TaskLoader
import com.online.job.test.helpers.LogHelper
import io.reactivex.schedulers.Schedulers.io

abstract class BaseActivity : AppCompatActivity(){

    val isOnline: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            val res = netInfo != null && netInfo.isConnected
            if(!res) UserPreferences.getInstance(applicationContext).needSynchronizeServer = true
            else if(UserPreferences.getInstance(applicationContext).needSynchronizeServer)
                runSynchronization()
           return res
    }


     private fun runSynchronization(){
        UserPreferences.getInstance(applicationContext).needSynchronizeServer = false
        io().scheduleDirect {
            LogHelper.d("runSynchronization","runSynchronization")
            updateState(DBLoader.STATE_NEED_CREATE)
            updateState(DBLoader.STATE_NEED_UPDATE)
            updateState(DBLoader.STATE_NEED_DELETE)

            TaskLoader.getTasks.onNext(false)
        }
    }

    private fun updateState(state : Int){
        val ids = DBLoader.getIdsByState(applicationContext, state)
        LogHelper.d("runSynchronization","updateState $state " + ids.size)
        for (id in ids){
            if(state != DBLoader.STATE_NEED_DELETE){
                val exTask = DBLoader.getExtendedTask(applicationContext,id)
                exTask?.task?.let {
                    when(state){
                        DBLoader.STATE_NEED_CREATE->TaskLoader.createTask(applicationContext, it)
                        DBLoader.STATE_NEED_UPDATE-> TaskLoader.editTask(it)
                    }
                    return@let
                }
            }else{
                TaskLoader.deleteTask(id)
            }

        }

        DBLoader.deleteAllWithState(applicationContext, state)
    }
}