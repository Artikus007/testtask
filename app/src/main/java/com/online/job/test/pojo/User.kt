package com.online.job.test.pojo

import com.google.gson.annotations.Expose

data class User (
    @Expose
    var email : String,
    @Expose
    var password : String
)