package com.online.job.test.pojo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class TaskContainer (
    var task : Task
)