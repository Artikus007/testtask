package com.online.job.test.dataloaders

import android.content.Context
import com.google.gson.Gson
import com.online.job.test.Constants
import com.online.job.test.helpers.LogHelper
import com.online.job.test.pojo.*
import io.reactivex.subjects.BehaviorSubject
import java.io.BufferedReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

object TaskLoader {

    @Volatile
    var offlineMode = false
    @Volatile
    var notifyingTasksOnly = false

    private var token = ""
    var sort = "dueBy"
    var sortOrder = "asc"
    var tasks = ArrayList<Task>()
    var errorMsg : Msg? = null
    var meta = Meta(1,1,1)
    val getTasks = BehaviorSubject.create<Boolean>()

    @Synchronized
    fun getTokenVal() : String{
        return  token
    }
    @Synchronized
    fun setTokenVal(v  : String){
         token = v
    }
    @Synchronized
    fun getTaskList() : ArrayList<Task>{
        return  tasks
    }
    @Synchronized
    fun setTaskList(  list :  ArrayList<Task>){
           tasks = list
    }
    @Synchronized
    fun reversSortOrder(){
        sortOrder = if(sortOrder == "asc") "desc" else "asc"
    }
    @Synchronized
    fun sortBy(  by : String){
        sort =  by
    }

    private fun buildSort() : String{
        return "&sort=$sort%20$sortOrder"
    }

    @Synchronized
    fun loadTasks(context: Context, isMore : Boolean) : Boolean{
        errorMsg = null

        if(notifyingTasksOnly){
            tasks = DBLoader.getNotifyTasks(context)
            notifyingTasksOnly = false
            return true
        }

        if(offlineMode){
            tasks = DBLoader.getTasks(context)
            return true
        }

        return   try {

                if(isMore  ){
                    if(meta.current* meta.limit < meta.count){
                        meta.current++
                    }else
                        return false
                }else
                    meta.current = 1

                val url =  Constants.TEST_API +  "tasks?page="+ meta.current + buildSort()

                val obj = URL(url)
                val con = obj.openConnection() as HttpURLConnection
                con.requestMethod = "GET"
                con.setRequestProperty("Content-Type","application/json")
                con.setRequestProperty("Authorization", "Bearer $token")
                LogHelper.d(this, "loadTasks code " + con.responseCode)


                 when(con.responseCode){
                    200 ->{
                        val data = con.inputStream.bufferedReader().use(BufferedReader::readText)

                        val res =  Gson().fromJson(data, Tasks::class.java)
                        if(!isMore)
                            setTaskList(res.tasks)
                        else
                            tasks.addAll(res.tasks)

                        meta = res.meta
                        true
                    }
                    401->{
                        val data = con.errorStream.bufferedReader().use(BufferedReader::readText)

                         errorMsg = Gson().fromJson(data, Msg::class.java)
                        false
                    }
                    500->{
                        errorMsg = Msg("Unauthorized",null)
                        val data = con.errorStream.bufferedReader().use(BufferedReader::readText)
                        LogHelper.d(this, "res $data" )
                        false
                    }
                    else->{
                        false
                    }
                }
        }catch (e : Exception){
            errorMsg = Msg(e.message?:"Error",null)
                false
        }
    }


    @Synchronized
    fun createTask(context : Context,  sendTask : Task) : Boolean{
        var task = sendTask
        LogHelper.d(this, "createTask "   )
        val url =  Constants.TEST_API +  "tasks"

        if(offlineMode){
            return true
        }

        val obj = URL(url)
        val con = obj.openConnection() as HttpURLConnection
        con.requestMethod = "POST"
        con.setRequestProperty("Content-Type","application/json")
        con.setRequestProperty("Authorization", "Bearer $token")
        con.setRequestProperty("accept","application/json")
        con.instanceFollowRedirects = true
        con.doOutput = true

        val json = Gson()
        val data = json.toJson(SendTask(task))
        val wr =  con.outputStream.bufferedWriter()
        wr.write(data)
        wr.flush()
        wr.close()
        LogHelper.d(this, "data $data")
        LogHelper.d(this, "code" + con.responseCode)
        errorMsg = null
        return when(con.responseCode){
            201 ->{
               //  LogHelper.d(this, "res " +  con.inputStream.bufferedReader().use(BufferedReader::readText))
                 val answer = json.fromJson(con.inputStream.buffered().bufferedReader().use(BufferedReader::readText), TaskContainer::class.java)
                 val oldId = task.id
                 task = answer.task
                 LogHelper.d(this, " extask.task " +    task.id)
                 DBLoader.updateTaskId(context,oldId,  task.id)
                true
            }
            403,422->{
                errorMsg = json.fromJson(con.errorStream.buffered().bufferedReader().use(BufferedReader::readText), Msg::class.java)
                false
            }
            else->{
                 errorMsg = Msg("Error",null)
                false
            }
        }
    }

    @Synchronized
    fun editTask(task : Task) : Boolean{
        if(offlineMode){
            return true
        }
        if(task.id.isEmpty()){
            errorMsg = Msg("Task id not exist",null)
            return false
        }

        val url =  Constants.TEST_API +  "tasks/" + task.id

        val obj = URL(url)
        val con = obj.openConnection() as HttpURLConnection
        con.requestMethod = "PUT"
        con.setRequestProperty("Content-Type","application/json")
        con.setRequestProperty("Authorization", "Bearer $token")
        con.setRequestProperty("accept","application/json")
        con.instanceFollowRedirects = true
        con.doOutput = true


        val json = Gson()
        val data = json.toJson(SendTask(task))
        val wr =  con.outputStream.bufferedWriter()
        wr.write(data)
        wr.flush()
        wr.close()
        LogHelper.d(this, "editTask taskId " + task.id )
        LogHelper.d(this, "code" + con.responseCode)

        errorMsg = null

        return when(con.responseCode){
            201,202 ->{
                LogHelper.d(this, "data " +  con.inputStream.bufferedReader().use(BufferedReader::readText))
                true
            }
            401,403,422->{
                errorMsg = json.fromJson(con.errorStream.buffered().bufferedReader().use(BufferedReader::readText), Msg::class.java)
                false
            }
            else->{
                errorMsg = Msg("Error",null)
                false
            }
        }
    }

    @Synchronized
    fun deleteTask(taskId : String) : Boolean{

        if(offlineMode){
            LogHelper.d(this, "deleteTask offlineMode " )
            return true
        }

        if(taskId.isEmpty()){
            errorMsg = Msg("Task id not exist",null)
            return false
        }

        val url =  Constants.TEST_API +  "tasks/" + taskId

        val obj = URL(url)
        val con = obj.openConnection() as HttpURLConnection
        con.requestMethod = "DELETE"
        con.setRequestProperty("Authorization", "Bearer " + TaskLoader.token)
        con.setRequestProperty("accept","application/json")
        con.instanceFollowRedirects = true

        LogHelper.d(this, "code " + con.responseCode)
        LogHelper.d(this, "deleteTask taskId $taskId"  )
        errorMsg = null

        return when(con.responseCode){
            202 ->{
                LogHelper.d(this, "token " +  con.inputStream.bufferedReader().use(BufferedReader::readText))
                true
            }
            401 ->{
                errorMsg = Gson().fromJson(
                    con.errorStream.buffered().bufferedReader().use(BufferedReader::readText), Msg::class.java)
                false
            }
            else->{
                errorMsg = Msg("Error",null)
                false
            }
        }
    }
}