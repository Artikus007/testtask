package com.online.job.test.activities

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.PopupMenu
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import com.online.job.test.R
import com.online.job.test.data.UserPreferences
import com.online.job.test.dataloaders.TaskLoader
import com.online.job.test.utils.toast
import kotlinx.android.synthetic.main.activity_tasks.*


class TasksActivity : BaseActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_tasks)

        TaskLoader.offlineMode =  !isOnline

        if (!isOnline)
        toast(this,"Offline")

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.navigationIcon?.setColorFilter(
                ContextCompat.getColor(this, R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP)
        }


        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
         btn_create_task.setOnClickListener {
            startActivity(Intent(this, EditTaskActivity::class.java))
        }
        btn_sort.setOnClickListener {
            val menu = PopupMenu(this, it, Gravity.END)
            menu.menu.add(Menu.NONE, MENU_SORT_BY_NAME, Menu.NONE,  getString(R.string.sort_name))
            menu.menu.add(Menu.NONE, MENU_SORT_BY_PRIORITY, Menu.NONE, getString(R.string.sort_priority))
            menu.menu.add(Menu.NONE, MENU_SORT_BY_DATE, Menu.NONE,  getString(R.string.sort_date))
            menu.setOnMenuItemClickListener(menuListener)
            menu.show()
        }
    }

    override fun onStart() {
        super.onStart()
        TaskLoader.offlineMode =  !isOnline
       if (TaskLoader.getTokenVal().isNotEmpty())
             TaskLoader.getTasks.onNext(false)
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_reverse_sort->{
                TaskLoader.reversSortOrder()
                TaskLoader.getTasks.onNext(false)
            }
            R.id.menu_notifying_tasks->{
                TaskLoader.notifyingTasksOnly = true
                TaskLoader.getTasks.onNext(false)
            }
            R.id.menu_settings->{
                startActivity(Intent(this, SettingsActivity::class.java))
            }

            R.id.menu_logout->{
                TaskLoader.setTokenVal("")
                UserPreferences.getInstance(applicationContext).userToken = ""
                val intent = Intent(this, LoginActivity::class.java)
                intent.action = TASK_EXIT
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private val menuListener = object : PopupMenu.OnMenuItemClickListener{
        override fun onMenuItemClick(item : MenuItem ): Boolean {
            when (item.itemId) {
                MENU_SORT_BY_NAME -> {
                    TaskLoader.sortBy("title")
                    TaskLoader.getTasks.onNext(false)
                    return true
                }
                MENU_SORT_BY_PRIORITY -> {
                    TaskLoader.sortBy("priority")
                    TaskLoader.getTasks.onNext(false)
                    return true
                }
                MENU_SORT_BY_DATE -> {
                    TaskLoader.sortBy("dueBy")
                    TaskLoader.getTasks.onNext(false)
                    return true
                }
            }
            return false
        }
    }

    companion object {
        const val MENU_SORT_BY_NAME = 1
        const val MENU_SORT_BY_PRIORITY = 2
        const val MENU_SORT_BY_DATE = 3
        val TASK_EXIT = EditTaskActivity::class.java.`package`?.name + ".EXIT"
    }
}
