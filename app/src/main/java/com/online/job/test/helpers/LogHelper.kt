/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.online.job.test.helpers

import android.util.Log
import com.online.job.test.BuildConfig


object LogHelper {

    private val debug = true and BuildConfig.DEBUG

    fun v(tag: String, messages: String?) {

        Log.v(tag, messages)
    }

    fun d(tag: String, messages: String?) {
        // Only log DEBUG if build type is DEBUG
        if (debug) {
            Log.d(tag, messages)
        }
    }

    fun e(tag: String, s: String?) {
        Log.e(tag, s)
    }


    fun d(o: Any, msg: String?) {
        if (debug) {
            Log.v(o.javaClass.simpleName, msg)
        }
    }

    fun v(  o : Any, msg: String?) {
        Log.v(o.javaClass.simpleName, msg)
    }

    fun i(o: Any, msg: String?) {
        Log.i(o.javaClass.simpleName, msg)
    }

    fun e(o: Any, msg: String?) {
        Log.e(o.javaClass.simpleName, msg)
    }
}