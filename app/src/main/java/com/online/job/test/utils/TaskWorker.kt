package com.online.job.test.utils

import android.app.NotificationManager
import android.app.NotificationChannel
import android.content.Context
import android.support.v4.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.online.job.test.R
import com.online.job.test.helpers.LogHelper


class TaskWorker(  context: Context, params :WorkerParameters) : Worker(context,params) {

    override fun doWork(): Result {
        LogHelper.d(this,"TaskWorker work")
        val title : String = inputData.getString(WORKER_TASK_NAME)?:"You have task on this time"
        val text = inputData.getString(WORKER_TASK_DESC)?:"Please go to the app"


        sendNotification(title, text)

        return Result.success()
    }

   private fun sendNotification(title: String, message: String) {

        val notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANEL_ID, CHANEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(applicationContext, CHANEL_ID)
            .setContentTitle(title)
            .setContentText(message)
            .setSmallIcon(R.mipmap.ic_launcher)

        notificationManager.notify(1, notification.build())
    }

    companion object {
          val CHANEL_ID =  TaskWorker::class.java.simpleName + ".CHANEL_ID"
          val CHANEL_NAME=  TaskWorker::class.java.simpleName + ".TASK_REMINDER"
          val WORKER_TASK_NAME =  TaskWorker::class.java.simpleName + ".WORKER_TASK_NAME"
          val WORKER_TASK_DESC =  TaskWorker::class.java.simpleName + ".WORKER_TASK_DESC"
    }
}