package com.online.job.test

object Constants {
    const val TEST_API = "http://testapi.doitserver.in.ua/api/"

    const val PRIORITY_HIGH = "High"
    const val PRIORITY_LOW = "Low"
    const val PRIORITY_NORMAL = "Normal"
}