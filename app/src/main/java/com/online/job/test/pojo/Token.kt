package com.online.job.test.pojo

import com.google.gson.annotations.Expose


data class Token (
    @Expose
    var token : String
)


