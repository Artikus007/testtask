package com.online.job.test.pojo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExtendedTask (
    var task : Task,
    val remind : Long,
    val desc : String
): Parcelable