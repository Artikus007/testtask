package com.online.job.test.dataloaders

import com.online.job.test.Constants
import com.online.job.test.pojo.SendTask
import com.online.job.test.pojo.Task
import com.online.job.test.pojo.Token
import com.online.job.test.pojo.User
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface TestApiInterface {

    @Headers("Content-Type:application/json" )
    @POST("auth")
    fun login(@Body user: User) : Call<Token>

    @Headers("Content-Type:application/json" )
    @POST("users")
    fun register(@Body user: User ) : Call<Token>


    @Headers("Content-Type:application/json")
    @GET("tasks")
    fun loadTasks(@Query("page") page : Int, @Query("sort") sort : String ) : Call<ArrayList<Task>>

    @Headers("Content-Type:application/json")
    @POST("tasks")
    fun createTask(@Body task : SendTask) : Call<Task>

    @Headers("Content-Type:application/json")
    @PUT("tasks/{taskId}")
    fun editTask(@Path("taskId") taskId : String , @Body task : SendTask) : Call<Task>

    @DELETE("tasks/{taskId}")
    fun deleteTask(@Path("taskId") taskId : String ) : Call<Any?>

    companion object  {
        fun create(client : OkHttpClient = OkHttpClient()): TestApiInterface {

            val retrofit = retrofit2.Retrofit.Builder()
                .client(client)
                .baseUrl(Constants.TEST_API)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return retrofit.create(TestApiInterface::class.java)

        }
        fun createWithToken() :  TestApiInterface  {
            return create(OkHttpClient
                .Builder()
                .addInterceptor{
                                var request = it.request()
                                val headers = request.headers().newBuilder()
                                    .add("Authorization", "Bearer " + TaskLoader.getTokenVal()).build()
                                request = request.newBuilder().headers(headers).build()
                                val response = it.proceed(request)
                                response
                            }
                .build())
        }

        fun build() : Retrofit  {
            return Retrofit.Builder()
                .baseUrl(Constants.TEST_API)
                .client(OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
        }
    }
}