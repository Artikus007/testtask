package com.online.job.test.fragments


import android.content.SharedPreferences
import android.os.Bundle
import android.support.v14.preference.SwitchPreference
import android.support.v7.preference.ListPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import com.online.job.test.R
import com.online.job.test.data.UserPreferences

class GeneralPreferenceFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    lateinit var mPreference: UserPreferences


    // @Inject
    // public PlaylistStore mPlaylistStore;

    private var firstRefresh = true

    override fun onCreate(savedInstanceState: Bundle?) {
        context?.let {
            mPreference = UserPreferences.getInstance(it)
        }
        super.onCreate(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {

    }

    override fun onCreatePreferences(bundle: Bundle?, rootKey: String?) {
         addPreferencesFromResource(R.xml.pref_general)


        val sharedPreferences = preferenceScreen.sharedPreferences
        val allEntries = sharedPreferences.all

        for ((key) in allEntries) {
            onSharedPreferenceChanged(mPreference.mPrefs, key)
        }
        firstRefresh = false

    }


    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences
                .registerOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        val preference = findPreference(key)
        if (preference != null)
            if (preference is ListPreference) {
                val prefIndex = preference.findIndexOfValue(sharedPreferences.getString(key, ""))
                if (prefIndex >= 0) {
                    preference.summary = preference.entries[prefIndex]
                }
            } else if (preference is SwitchPreference) {
                preference.summary = sharedPreferences.getBoolean(key, false).toString()
            }    else {
                preference.summary = sharedPreferences.getString(key, "")
           }

    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences
                .unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onPreferenceTreeClick(preference: Preference): Boolean {
        when (preference.key  ) {

        }
        return super.onPreferenceTreeClick(preference)
    }


}
