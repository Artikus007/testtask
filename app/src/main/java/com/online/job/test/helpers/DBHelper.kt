package com.online.job.test.helpers

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context)
    : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(
                "create table IF NOT EXISTS $TASK_TABLE  ("
                        + "id INTEGER primary key autoincrement,"
                        + ID + " LONG, "
                        + TITLE + " VARCHAR(255), "
                        + DUE_BY + " VARCHAR(255), "
                        + PRIORITY + " VARCHAR(255), "
                        + REMIND + " LONG, "
                        + DESC + " TEXT "
                        + ");"
                )
        db.execSQL(
            "create table IF NOT EXISTS $STATE_TABLE  ("
                    + "id INTEGER primary key autoincrement,"
                    + ID + " LONG, "
                    + STATE + " INTEGER "
                    + ");"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    companion object {

        const val ID = "_id"
        const val TITLE = "title"
        const val DUE_BY = "due_by"
        const val PRIORITY = "priority"
        const val REMIND = "remind_time"
        const val DESC = "desc"
        const val STATE = "state"

        const val DB_VERSION = 1
        const val DB_NAME = "TaskData"
        const val TASK_TABLE = "tasks"
        const val STATE_TABLE = "states"
    }
}
