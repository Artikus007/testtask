package com.online.job.test.data

import android.content.Context
import android.content.SharedPreferences
import android.support.annotation.StringRes
import android.support.v7.preference.PreferenceManager
import com.online.job.test.R
import com.online.job.test.utils.SingletonHolder

class  UserPreferences(private val mContext: Context) {
    var mPrefs : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)


    @Synchronized
    private fun getBoolean(@StringRes keyRes: Int, defaultValue: Boolean): Boolean {
        return mPrefs.getBoolean(mContext.getString(keyRes), defaultValue)
    }

    @Synchronized
    private fun getInt(@StringRes keyRes: Int, defaultValue: Int): Int {
        var i = 0
        try {
            i = mPrefs.getInt(mContext.getString(keyRes), defaultValue)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return i
    }

    @Synchronized
    private fun getLong(@StringRes keyRes: Int, defaultValue: Long): Long {
        var l: Long = 0
        try {
            l = mPrefs.getLong(mContext.getString(keyRes), defaultValue)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return l
    }

    @Synchronized
    private fun getString(@StringRes keyRes: Int, defaultValue: String): String {
        var s = ""
        try {
            s = mPrefs.getString(mContext.getString(keyRes), defaultValue)?:""
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return s
    }


    @Synchronized
    private fun putBoolean(@StringRes keyRes: Int, value: Boolean) {
        try {
            mPrefs.edit()
                .putBoolean(mContext.getString(keyRes), value)
                .apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Synchronized
    private fun putInt(@StringRes keyRes: Int, value: Int) {
        try {
            mPrefs.edit()
                .putInt(mContext.getString(keyRes), value)
                .apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Synchronized
    private fun putLong(@StringRes keyRes: Int, value: Long) {
        try {
            mPrefs.edit()
                .putLong(mContext.getString(keyRes), value)
                .apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Synchronized
    private fun putString(@StringRes keyRes: Int, value: String) {
        try {
            mPrefs.edit()
                .putString(mContext.getString(keyRes), value)
                .apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    var userEmail: String
        get() = getString(R.string.pref_key_email, "")
        set(more) = putString(R.string.pref_key_email, more)

    var userPass: String
        get() = getString(R.string.pref_key_pass, "")
        set(more) = putString(R.string.pref_key_pass, more)

    var userToken: String
        get() = getString(R.string.pref_key_token, "")
        set(more) = putString(R.string.pref_key_token, more)

    var needSynchronizeServer: Boolean
        get() = getBoolean(R.string.pref_key_synchronize, false)
        set(more) = putBoolean(R.string.pref_key_synchronize, more)

    val sortOrderAscending: Boolean
        get() = getBoolean(R.string.pref_key_sort_order, false)


    companion object : SingletonHolder<UserPreferences, Context>( {
        UserPreferences(it.applicationContext)
    })
}
