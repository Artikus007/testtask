package com.online.job.test.pojo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Task (
    var id: String,
    var title: String,
    var dueBy: String,
    var priority: String
): Parcelable