package com.online.job.test.pojo

data class Tasks(
    val tasks: ArrayList<Task>,
    val meta: Meta
)