package com.online.job.test.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.online.job.test.R
import com.online.job.test.activities.TaskInfoActivity
import com.online.job.test.pojo.Task
import com.online.job.test.utils.getDateTime
import kotlinx.android.synthetic.main.instance_task.view.*
import java.util.*

class TaskListAdapter(var tasks : Vector<Task> = Vector()) : RecyclerView.Adapter<TaskHolder>(){


    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int): TaskHolder {
        return TaskHolder(LayoutInflater.from(parent.context).inflate(R.layout.instance_task, parent, false))
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    override fun onBindViewHolder(holder : TaskHolder, position: Int) {
        if(tasks.size > 0 && position < tasks.size){
            holder.onBind(tasks[position])
        }
    }

}

class TaskHolder(private val view : View): RecyclerView.ViewHolder(view){

    fun onBind(task : Task){
        view.title.text = task.title
        view.date.text = getDateTime(task.dueBy)
        view.priority.text = task.priority
        view.setOnClickListener {
            val context = view.context
            val intent = Intent(context,TaskInfoActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(TaskInfoActivity.TASK_ITEM, task)
            context.startActivity(intent)
        }
    }

}

