package com.online.job.test.activities

import android.app.TimePickerDialog
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import com.online.job.test.R
import com.online.job.test.dataloaders.TaskLoader
import com.online.job.test.pojo.Task
import com.online.job.test.utils.toast
import kotlinx.android.synthetic.main.activity_edit_task.*
import java.util.*
import android.app.DatePickerDialog
import android.content.Intent
import com.online.job.test.Constants
import com.online.job.test.activities.TaskInfoActivity.Companion.TASK_ITEM
import com.online.job.test.dataloaders.DBLoader
import com.online.job.test.dialogs.TimeRemindDialogFragment
import com.online.job.test.pojo.ExtendedTask
import com.online.job.test.utils.getDate
import com.online.job.test.utils.getTime
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers.io
import java.lang.Exception
import java.text.SimpleDateFormat
import androidx.work.OneTimeWorkRequest
import androidx.work.Data
import androidx.work.WorkManager
import com.online.job.test.utils.TaskWorker
import java.util.concurrent.TimeUnit




class EditTaskActivity : BaseActivity()  {
    private var task = Task("","","",Constants.PRIORITY_NORMAL)
    private var edit = false
    private var mYear: Int = 0
    private var mMonth: Int = 0
    private var mDay: Int = 0
    private var mHour: Int = 0
    private var mMinute: Int = 0
    private var mReminderTime: Long = -1
    private var is24Hour = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_task)

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.navigationIcon?.setColorFilter(
                ContextCompat.getColor(this, R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP)
        }
        toolbar.setNavigationOnClickListener { finish()}




        intent.extras?.get(TASK_EDIT)?.let {
                task = it as Task
                toolbar_title.text = getString(R.string.title_edit_task)
                title_task.setText(task.title)
                btn_delete_create.text = getString(R.string.title_delete_task)
                btn_edit.visibility = View.VISIBLE
                btn_edit.setOnClickListener { editTask() }
                date.setText(getDate(task.dueBy))
                datetime.setText(getTime(task.dueBy))

                val extendedTask = DBLoader.getExtendedTask(applicationContext,task.id)
                extendedTask?.let { extTask->
                    desc.setText(extTask.desc)
                    if(extTask.remind  > 0){
                        notify_before.text = getString(R.string.remind_me_before , extTask.remind)
                        switch_notify.isChecked = true
                        notify_before.isEnabled = true
                    } else if(extTask.remind  == 0L){
                        notify_before.text = getString(R.string.remind_me)
                        switch_notify.isChecked = true
                        notify_before.isEnabled = true
                    }else{
                        notify_before.text = getString(R.string.not_remind_me)
                        switch_notify.isChecked = false
                        notify_before.isEnabled = false
                    }
                        mReminderTime =  extTask.remind
                }
                edit = true
        }

        btn_delete_create.setOnClickListener {
            if(edit)
                deleteTask()
            else
                createTask()
        }


        btn_high.setOnClickListener {
            notifyPriority(Constants.PRIORITY_HIGH)
        }

        btn_low.setOnClickListener {
            notifyPriority(Constants.PRIORITY_LOW)
        }

        btn_medium.setOnClickListener {
            notifyPriority(Constants.PRIORITY_NORMAL)
        }

        datetime.setOnClickListener {
            val c = Calendar.getInstance()
            mHour = c.get(Calendar.HOUR_OF_DAY)
            mMinute = c.get(Calendar.MINUTE)
            val timePickerDialog = TimePickerDialog(this,
                TimePickerDialog.OnTimeSetListener { dialog, hourOfDay, minute ->
                    is24Hour = dialog.is24HourView
                    datetime.setText(String.format( "%d:%d" ,hourOfDay,minute))
                },
                mHour,
                mMinute,
                is24Hour
            )
            timePickerDialog.show()
        }
        date.setOnClickListener {
            val c = Calendar.getInstance()
            mYear = c.get(Calendar.YEAR)
            mMonth = c.get(Calendar.MONTH)
            mDay = c.get(Calendar.DAY_OF_MONTH)
            val datePickerDialog = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    date.setText(String.format( "%d-%d-%d" ,dayOfMonth,monthOfYear+1,year))
                },
                mYear,
                mMonth,
                mDay
            )
            datePickerDialog.datePicker.minDate = System.currentTimeMillis()
            datePickerDialog.show()
        }
        notify_before.setOnClickListener {
            TimeRemindDialogFragment().show(supportFragmentManager)
        }

        switch_notify.setOnClickListener {
            val checked = switch_notify.isChecked
            notify_before.isEnabled = checked
            if(checked){
                if(mReminderTime < 0){
                    mReminderTime = 0
                    notify_before.text = getString(R.string.remind_me)
                }
            }else{
                mReminderTime = -1
                notify_before.text = getString(R.string.not_remind_me)
            }
        }
        notifyPriority(task.priority)

    }

    private fun changeTask(type : Int){
        if(getTimeFromDate())
        io().scheduleDirect {
                  task.title = title_task.text.toString()
                    val exTask = ExtendedTask(
                        task,
                        mReminderTime,
                        desc.text.toString()
                    )
                   val result = try {
                       when (type) {
                              CREATE_TASK -> {
                                  DBLoader.addTask(applicationContext,exTask)
                                  val res= TaskLoader.createTask(applicationContext, task)
                                  if(res)createWorker(exTask)
                                  res
                              }
                              EDIT_TASK -> {
                                  DBLoader.updateTask(applicationContext, exTask)
                                  val res=  TaskLoader.editTask( task )
                                  if(res){
                                      deleteWorker(task.id)
                                      createWorker(exTask)
                                  }
                                  res
                              }
                              else -> {
                                  DBLoader.deleteTask(applicationContext, task.id)
                                  deleteWorker(task.id)
                                  TaskLoader.deleteTask(task.id )
                              }
                          }
                  }catch (e :Exception){
                      e.printStackTrace()
                      false
                  }
                 mainThread().scheduleDirect {
                      if (result) {
                          goBack()
                      } else {
                          TaskLoader.errorMsg?.let { error ->
                              toast(this, error.message)
                              title_task.error = error.fields?.title?.joinToString()
                          }
                      }
                  }
           }
    }
    private fun editTask() = changeTask(EDIT_TASK)
    private fun createTask() = changeTask(CREATE_TASK)
    private fun deleteTask() = changeTask(DELETE_TASK)

    private fun goBack(){
        if(edit){
            val intent = Intent(this, TaskInfoActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(TASK_ITEM, task)
            startActivity(intent)
        }else  {
            val intent = Intent(this, TasksActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

    }

    private fun notifyPriority(v : String){
        task.priority = v
        btn_high.isSelected = false
        btn_low.isSelected = false
        btn_medium.isSelected = false
        when( task.priority){
             Constants.PRIORITY_HIGH ->btn_high.isSelected = true
             Constants.PRIORITY_LOW-> btn_low.isSelected = true
             Constants.PRIORITY_NORMAL->btn_medium.isSelected = true
        }
    }


    private fun getTimeFromDate() : Boolean{
        val strTime = datetime.text.toString()
        var err = false
        if(strTime.isEmpty()){
            timeLayout.error = "Empty field"
            err = true
        }
        val strDate = date.text.toString()
        if(strDate.isEmpty()){
            dateLayout.error = "Empty field"
            err = true
        }
        if (err)return false

        val strDateTime = "$strDate $strTime"
        val formatter = if(is24Hour)
            SimpleDateFormat("dd-MM-yyyy HH:mm",Locale.getDefault())
        else
            SimpleDateFormat("dd-MM-yyyy hh:mm",Locale.getDefault())

        val mDate = formatter.parse(strDateTime) as Date
        val timestamp = mDate.time/1000

        return if(timestamp < System.currentTimeMillis()/1000){
             timeLayout.error = "This date has already passed"
             dateLayout.error = "This date has already passed"
             false
        }else{
            task.dueBy = timestamp.toString()
            true
        }

    }

    fun handleRemindPos( pos : Int ){
        mReminderTime = 5L*(pos+1)
        notify_before.text = String.format("%d min before" , mReminderTime)
    }


    private fun createWorker(task : ExtendedTask){
        if(mReminderTime < 0) return
        var delay = (task.task.dueBy.toLong()*1000)-System.currentTimeMillis()

        delay -= mReminderTime * 60 * 1000
        val data = Data.Builder()
            .putString(TaskWorker.WORKER_TASK_NAME, task.task.title)
            .putString(TaskWorker.WORKER_TASK_DESC, task.desc)
            .build()

        val simpleRequest = OneTimeWorkRequest.Builder(TaskWorker::class.java)
            .setInitialDelay(delay, TimeUnit.MILLISECONDS)
            .setInputData(data)
            .addTag(task.task.id)
            .build()
        WorkManager.getInstance().enqueue(simpleRequest)
    }

    private  fun deleteWorker(id : String){
        WorkManager.getInstance().cancelAllWorkByTag(id)
    }



    companion object {
        val TASK_EDIT = EditTaskActivity::class.java.`package`?.name + ".TASK_EDIT"
        const val CREATE_TASK = 0
        const val DELETE_TASK = 1
        const val EDIT_TASK = 2
    }
}
