package com.online.job.test.pojo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SendTask (
    var title: String,
    var dueBy: String,
    var priority: String
): Parcelable{
    constructor(task : Task) :  this(task.title, task.dueBy,task.priority)
}