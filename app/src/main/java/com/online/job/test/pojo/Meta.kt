package com.online.job.test.pojo

data class Meta(
    val count: Int,
    var current: Int,
    val limit: Int
)