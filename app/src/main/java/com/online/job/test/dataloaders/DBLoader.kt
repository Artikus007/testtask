package com.online.job.test.dataloaders

import android.content.ContentValues
import android.content.Context
import com.online.job.test.helpers.DBHelper
import com.online.job.test.helpers.DBHelper.Companion.ID
import com.online.job.test.helpers.DBHelper.Companion.REMIND
import com.online.job.test.helpers.DBHelper.Companion.STATE
import com.online.job.test.helpers.DBHelper.Companion.STATE_TABLE
import com.online.job.test.helpers.DBHelper.Companion.TASK_TABLE
import com.online.job.test.helpers.DBHelper.Companion.TITLE
import com.online.job.test.helpers.LogHelper
import com.online.job.test.pojo.ExtendedTask
import com.online.job.test.pojo.Task
import kotlin.collections.ArrayList

object DBLoader{

    const val STATE_NEED_CREATE = 1
    const val STATE_NEED_UPDATE = 2
    const val STATE_NEED_DELETE = 3


    fun getExtendedTask(context: Context, id :String): ExtendedTask? {
         try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase
            val c = db.query(TASK_TABLE, arrayOf("*"), "$ID = $id",
                null , null, null, null)
             LogHelper.d(this,"getExtendedTask $ID = $id "  )

            if (c != null && c.moveToFirst()) {
                val idIndex = c.getColumnIndex(ID)
                val title = c.getColumnIndex(TITLE)
                val dueBy = c.getColumnIndex(DBHelper.DUE_BY)
                val priority = c.getColumnIndex(DBHelper.PRIORITY)
                val remind = c.getColumnIndex(DBHelper.REMIND)
                val desc = c.getColumnIndex(DBHelper.DESC)
                LogHelper.d(this,"getExtendedTask  true"  )
                return  ExtendedTask(
                            Task(
                            c.getLong(idIndex).toString(),
                            c.getString(title),
                            c.getString(dueBy),
                            c.getString(priority)
                        ),
                   c.getLong(remind),
                   c.getString(desc))
            }
            c.close()
            db.close()
            dbHelper.close()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }
    fun getNotifyTasks(context: Context  ): ArrayList<Task> {
        return getTasks(context, " $REMIND >= 0 ")
    }
    fun getTasks(context: Context , selection : String? = null): ArrayList<Task> {
        val list = ArrayList<Task>()
        try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase
            val c = db.query(TASK_TABLE, arrayOf("*"),  selection, null , null, null, null)

            if (c != null && c.moveToFirst()) {
                LogHelper.d(this, "getTasks " + c.count )
                val idIndex = c.getColumnIndex(ID)
                val title = c.getColumnIndex(TITLE)
                val dueBy = c.getColumnIndex(DBHelper.DUE_BY)
                val priority = c.getColumnIndex(DBHelper.PRIORITY)
                val remind = c.getColumnIndex(DBHelper.REMIND)
                do{
                    list.add(
                        Task(
                            c.getLong(idIndex).toString(),
                            c.getString(title),
                            c.getString(dueBy),
                            c.getString(priority)
                        ) )
                    LogHelper.d(this, "remind " + c.getLong(remind) )
                }while ( (c.moveToNext()))

            }
            c.close()
            db.close()
            dbHelper.close()

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return list
    }


    fun addTask(context: Context, task :  ExtendedTask ) {

        try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase

                LogHelper.d(this,"addTasks $ID = " + task.task.id  )

                var cv = ContentValues()
                cv.put(ID, task.task.id)
                cv.put(TITLE, task.task.title)
                cv.put(DBHelper.DUE_BY, task.task.dueBy)
                cv.put(DBHelper.PRIORITY,task.task.priority)
                cv.put(DBHelper.REMIND, task.remind)
                cv.put(DBHelper.DESC, task.desc)


                val index = db.insert(TASK_TABLE, null, cv)

                LogHelper.d(this,"addTasks index = $index " + task.task.id.isEmpty() + TaskLoader.offlineMode )

                val tempId = index*-1L
                if(index != -1L && task.task.id.isEmpty()){

                    cv = ContentValues()
                    cv.put(ID, tempId)
                    db.update(TASK_TABLE,cv,"id = $index" , null)
                    task.task.id  = tempId.toString()

                    LogHelper.d(this,"update $ID = $tempId" )
                }
                if(TaskLoader.offlineMode)
                updateState(context,tempId,STATE_NEED_CREATE)

            db.close()
            dbHelper.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    fun updateTask(context: Context, task:ExtendedTask) {
        updateTasks(context, arrayListOf(task))
    }

    fun updateTaskId(context: Context, oldId : String, newId : String ) {
        try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase

            val cv = ContentValues()
            cv.put(ID, newId)
            LogHelper.d(this,"updateTaskId $oldId $newId" )
            val res =   db.update(TASK_TABLE,  cv, " $ID = $oldId " , null)

            LogHelper.d(this,"updateTaskId res $res  " )

            db.close()
            dbHelper.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun updateTasks(context: Context, tasks: ArrayList<ExtendedTask> ) {
        try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase

            for (i in tasks.indices) {

                val cv = ContentValues()
                cv.put(TITLE, tasks[i].task.title)
                cv.put(DBHelper.DUE_BY, tasks[i].task.dueBy)
                cv.put(DBHelper.PRIORITY, tasks[i].task.priority)
                cv.put(DBHelper.REMIND, tasks[i].remind)
                cv.put(DBHelper.DESC, tasks[i].desc)

                db.update(TASK_TABLE,  cv, "$ID = ?" , arrayOf(tasks[i].task.id))


                if( TaskLoader.offlineMode){
                    updateState(context, tasks[i].task.id.toLong(), STATE_NEED_UPDATE)
                }
            }

            db.close()
            dbHelper.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun deleteTask(context: Context, id : String) {
        try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase
            db.delete(TASK_TABLE," $ID = $id ",null)
            LogHelper.d(this,"deleteTask $ID = $id  " )

            if( TaskLoader.offlineMode){
                updateState(context, id.toLong(), STATE_NEED_DELETE)
            }
            db.close()
            dbHelper.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun updateState(context: Context, id : Long, state : Int) {
        LogHelper.d(this,"updateState  id = $id  state = $state" )
        try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase
            val c = db.query(STATE_TABLE, arrayOf("*"),  " $ID = $id AND $STATE = $state", null , null, null, null)

            if (c == null || !c.moveToFirst()) {
                val cv = ContentValues()
                cv.put(STATE, state)
                cv.put(ID, id)
                val res =  db.insert(STATE_TABLE,null,cv)
                LogHelper.d(this,"updateState  insert res = $res" )
            }else
                LogHelper.d(this,"updateState  error" )
            c.close()
            db.close()
            dbHelper.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getIdsByState(context: Context, state: Int) : ArrayList<String> {
        val idList = ArrayList<String>()
        try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase
            val c = db.query(STATE_TABLE, arrayOf("*"),  "$STATE = $state", null , null, null, null)

            if (c != null && c.moveToFirst()) {
                LogHelper.d(this,"updateState  moveToFirst state $state" )
                val idIndex = c.getColumnIndex(ID)
                do {
                    idList.add(c.getLong(idIndex).toString())
                }while (c.moveToNext())
            }
            c.close()
            db.close()
            dbHelper.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
       return  idList
    }
    fun deleteState(context: Context, id : Long, state : Int) {
        try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase
            db.delete(STATE_TABLE,  " $ID = $id AND $STATE = $state", null )
            db.close()
            dbHelper.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun deleteAllWithState(context: Context, state : Int) {
        try {
            val dbHelper = DBHelper(context)
            val db = dbHelper.writableDatabase
            db.delete(STATE_TABLE,  " $STATE = $state ", null )
            db.close()
            dbHelper.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}