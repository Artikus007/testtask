package com.online.job.test.pojo

import com.google.gson.annotations.Expose

data class Fields (
    @Expose
    var email : ArrayList<String>?,
    @Expose
    var password : ArrayList<String>?,
    @Expose
    var title : ArrayList<String>?
 )