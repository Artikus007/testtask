package com.online.job.test.dataloaders

import com.google.gson.Gson
import com.online.job.test.helpers.LogHelper
import com.online.job.test.pojo.*
import io.reactivex.schedulers.Schedulers.io
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Response
import retrofit2.Call
import retrofit2.Callback

object UserLoader {
    const val START = 1
    const val END = 2
    const val ERR = 3
    const val WAIT = 0

    var  error : Msg? = null

    val  loading  = BehaviorSubject.create<Int>()

    fun loginUser(user : User, isRegister : Boolean) {
            loading.onNext(START)
            io().scheduleDirect {
                val  api = TestApiInterface.create()
                if(isRegister) {
                    val call = api.register(user )
                    call.enqueue(callbackToken)
                } else {
                    val call = api.login(user )
                    call.enqueue(callbackToken)
                }
            }
    }
     private  val callbackToken = object  : Callback<Token>  {
          override fun onFailure(call: Call<Token>, t: Throwable) {
              LogHelper.d(UserLoader, "error " + t.message)
              handleError(t.message)
          }
          override fun onResponse(call: Call<Token>, response:  Response<Token>) {
              error =  null
              when(response.code()){
                  200,201 ->{
                      response.body()?.token?.let {
                          TaskLoader.setTokenVal(it)
                          LogHelper.d(UserLoader, "token $it" )
                          loading.onNext(END)
                      }
                  } else-> handleError(response.errorBody()?.string())
              }
           }
      }



    private fun handleError(err : String?){
        error =  err?.let {
            try {
                Gson().fromJson(it, Msg::class.java)
            }catch (e : Exception){
                null
            } 
        }?:
            Msg("Error")
        loading.onNext(ERR)
    }

}