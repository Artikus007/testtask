package com.online.job.test.activities

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import com.online.job.test.R
import com.online.job.test.activities.EditTaskActivity.Companion.TASK_EDIT
import com.online.job.test.dataloaders.DBLoader
import com.online.job.test.dataloaders.TaskLoader
import com.online.job.test.pojo.Task
import com.online.job.test.utils.getDateTime
import com.online.job.test.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers.*
import kotlinx.android.synthetic.main.activity_task_info.*


class TaskInfoActivity : BaseActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_info)


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            info_toolbar.navigationIcon?.setColorFilter(
                ContextCompat.getColor(this, R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP)
        }
        info_toolbar.setNavigationOnClickListener { finish()}


        intent.extras?.get( TASK_ITEM)?.let {
            val task = it as Task
            task_title.text = task.title

            task_date.text = getDateTime(task.dueBy)
            priority.text = task.priority

            btn_delete.setOnClickListener {
                io().scheduleDirect {
                    DBLoader.deleteTask(applicationContext, task.id)
                    val result =  TaskLoader.deleteTask(task.id)
                    AndroidSchedulers.mainThread().scheduleDirect {
                        if (result) {
                            finish()
                        } else {
                            TaskLoader.errorMsg?.let { error ->
                                toast(this, error.message)
                            }
                        }
                    }
                }
            }
            btn_edit_task.setOnClickListener {
                val intent = Intent(this,EditTaskActivity::class.java)
                intent.putExtra(TASK_EDIT, task)
                startActivity(intent)
            }
            val extendedTask = DBLoader.getExtendedTask(applicationContext,task.id)
            extendedTask?.let { extTask->

                desc.text = extTask.desc
                if(extTask.remind  > 0){
                    notification.text = getString(R.string.remind_me_before , extTask.remind)
                } else if(extTask.remind  == 0L){
                    notification.text = getString(R.string.remind_me)
                }else{
                    notification.text = getString(R.string.not_remind_me)
                }
            }
        }
    }

    companion object {
          val TASK_ITEM = TaskInfoActivity::class.java.`package`?.name + ".TASK_ITEM"
    }
}
