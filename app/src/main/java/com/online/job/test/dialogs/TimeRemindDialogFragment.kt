package com.online.job.test.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import com.online.job.test.R
import com.online.job.test.activities.EditTaskActivity


class TimeRemindDialogFragment : BaseDialogFragment()  {


    private val mItemsSelect : Array<String?>
     get() {
        val array = arrayOfNulls<String>(12)
        for (i in  0..11 ) {
           array[i] = ((i+1)*5).toString() + " min"
        }
        return  array
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return context?.let {

          AlertDialog.Builder(it)
                    .setTitle(R.string.title_remind_dialog)
                    .setItems(mItemsSelect){_, pos :Int->
                        activity?.let {fragmentActivity->
                            try {
                                val taskEditTaskActivity = fragmentActivity as EditTaskActivity
                                taskEditTaskActivity.handleRemindPos(pos)
                            } catch (e: ClassCastException) {
                                e.printStackTrace()
                            }
                            dismiss()
                        }
                    }
                    .setNegativeButton(R.string.action_cancel, null)
                    .show()


        }?:super.onCreateDialog(savedInstanceState)
    }


}
