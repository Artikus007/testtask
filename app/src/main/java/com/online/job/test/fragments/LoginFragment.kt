package com.online.job.test.fragments

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Switch
import com.online.job.test.R
import com.online.job.test.activities.TasksActivity
import com.online.job.test.data.UserPreferences
import com.online.job.test.dataloaders.TaskLoader
import com.online.job.test.dataloaders.UserLoader
import com.online.job.test.dataloaders.UserLoader.END
import com.online.job.test.dataloaders.UserLoader.ERR
import com.online.job.test.dataloaders.UserLoader.START
import com.online.job.test.dataloaders.UserLoader.WAIT
import com.online.job.test.helpers.LogHelper
import com.online.job.test.helpers.UserHelper
import com.online.job.test.pojo.User
import com.online.job.test.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.fragment_login.view.*


class LoginFragment : BaseFragment()  {

    private lateinit var email : EditText
    private lateinit var password : EditText
    private lateinit var emailForm : TextInputLayout
    private lateinit var passwordForm : TextInputLayout
    private lateinit var progressBar : ProgressBar
    private lateinit var loginBtn : Button
    private lateinit var switchToReg : Switch

    private var user = User("","")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val v = LayoutInflater.from(context).inflate(R.layout.fragment_login, container, false)
        email = v.email
        emailForm = v.email_login_form
        password = v.password
        passwordForm = v.password_login_form
        progressBar = v.login_progress
        loginBtn = v.email_sign_in_button
        loginBtn.setOnClickListener{attemptLogin()}
        switchToReg = v.switchToReg

        context?.let {
            UserPreferences.getInstance(it).let {prefs->
                email.setText(prefs.userEmail)
                password.setText(prefs.userPass)
            }
        }
        LogHelper.d(this,"init  ")
        UserLoader.loading.onNext(WAIT)

        disposable.add(UserLoader.loading
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({state->
                LogHelper.d(this,"state $state")
                when(state){
                    START->  {
                        showProgress(true)
                    }
                    END->{
                        LogHelper.d(this,"goNext")
                        showProgress(false)
                        goNext()
                    }
                    ERR->{
                        showProgress(false)
                        checkError()
                    }
                }
            },{e->e.printStackTrace()})
        )
        return v
    }

    private fun goNext(){
        context?.let {
            UserPreferences.getInstance(it).let {prefs->
                prefs.userEmail = user.email
                prefs.userPass = user.password
                prefs.userToken = TaskLoader.getTokenVal()
                startActivity(Intent(context, TasksActivity::class.java))
            }
        }

    }


    private fun checkError() {
        UserLoader.error?.let {
            toast(context,it.message)
        }
        emailForm.error = UserLoader.error?.fields?.email?.joinToString()
        passwordForm.error = UserLoader.error?.fields?.password?.joinToString()
    }


    private fun attemptLogin() {

        emailForm.error = null
        passwordForm.error = null

        user.email = email.text.toString()
        user.password = password.text.toString()

        if (!UserHelper.isPasswordValid(user.password)  ) {
            passwordForm.error = getString(R.string.error_invalid_password)
        }

        when(UserHelper.isEmailValid( user.email)){
            UserHelper.EMPTY_EMAIL->emailForm.error = getString(R.string.error_field_required)
            UserHelper.INVALID_EMAIL ->emailForm.error = getString(R.string.error_invalid_email)
        }

        if(emailForm.error == null && passwordForm.error == null){
            showProgress(true)
            UserLoader.loginUser(user,   switchToReg.isChecked)
        }
    }




    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
          progressBar.visibility = if (show) View.VISIBLE else View.INVISIBLE
    }
}