package com.online.job.test

import android.app.Application
import com.online.job.test.data.UserPreferences
import com.online.job.test.dataloaders.TaskLoader

class App : Application(){

    override fun onCreate() {
        super.onCreate()
        val prefs = UserPreferences.getInstance(applicationContext)
        TaskLoader.sortOrder = if(prefs.sortOrderAscending)  "asc"  else "desc"
    }
}