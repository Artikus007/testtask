package com.online.job.test.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.online.job.test.R
import com.online.job.test.activities.LoginActivity
import com.online.job.test.activities.TasksActivity.Companion.TASK_EXIT
import com.online.job.test.adapters.TaskListAdapter
import com.online.job.test.data.UserPreferences
import com.online.job.test.dataloaders.TaskLoader
import com.online.job.test.helpers.LogHelper
import com.online.job.test.pojo.Task
import com.online.job.test.utils.MyLinearLayoutManager
import com.online.job.test.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.list.view.*

class TaskListFragment : BaseFragment() {

      lateinit var recyclerView: RecyclerView

      lateinit var refreshLayout: SwipeRefreshLayout

      private val mAdapter = TaskListAdapter()

      val lastVisibleItemPosition  : Int   get()  {
          val linearLayoutManager = recyclerView.layoutManager as MyLinearLayoutManager
          return linearLayoutManager.findLastVisibleItemPosition()
      }

      override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

          val view = LayoutInflater.from(context).inflate(R.layout.fragment_tasks, container, false)

          recyclerView =  view.list
          refreshLayout = view.refreshLayout

          refreshLayout.setColorSchemeColors(  resources.getColor(R.color.colorAccent) )
          refreshLayout.setOnRefreshListener{
              LogHelper.d(this,"Refresh")
              TaskLoader.getTasks.onNext(false)
          }

          recyclerView.layoutManager = MyLinearLayoutManager(context)

          recyclerView.adapter = mAdapter

          disposable.add(TaskLoader.getTasks
              .observeOn(mainThread())
              .map{
                  refreshLayout.isRefreshing = true
                  it
              }
              .observeOn(Schedulers.io())
              .map {isMore->
                  context?.let {
                      TaskLoader.loadTasks(it,isMore)
                  } ?:
                  false
              }
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe({success->
                  if(success)
                      notifyAdapter(TaskLoader.getTaskList())
                  else {
                      TaskLoader.errorMsg?.let {msg->
                          toast(context,msg.message)
                          if (msg.message == "Unauthorized"){
                              activity?.let {
                                  UserPreferences.getInstance(it.applicationContext).userToken = ""
                                  TaskLoader.setTokenVal("")
                                  val intent = Intent(it, LoginActivity::class.java)
                                  intent.action = TASK_EXIT
                                  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                  it.startActivity(intent)
                              }
                          }
                      }
                      recyclerView.clearOnScrollListeners()
                  }
                  refreshLayout.isRefreshing = false
              },{e->e.printStackTrace()})
          )

          return view
      }

    private val scrollListener = object : RecyclerView.OnScrollListener(){

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            val totalItemCount = recyclerView.layoutManager?.itemCount
            if (totalItemCount == lastVisibleItemPosition + 1) {
                recyclerView.clearOnScrollListeners()
                TaskLoader.getTasks.onNext(true)
            }
        }
    }


    @Synchronized
    private fun notifyAdapter(items : ArrayList<Task>){
        mAdapter.tasks.clear()
        mAdapter.tasks.addAll(items)
        mAdapter.notifyDataSetChanged()

        recyclerView.clearOnScrollListeners()
        recyclerView.addOnScrollListener(scrollListener)
    }


  }