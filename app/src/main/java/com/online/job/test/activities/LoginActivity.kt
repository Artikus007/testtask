package com.online.job.test.activities

import android.content.Intent
import android.os.Bundle
import com.online.job.test.R
import com.online.job.test.activities.TasksActivity.Companion.TASK_EXIT
import com.online.job.test.data.UserPreferences
import com.online.job.test.dataloaders.TaskLoader


class LoginActivity : BaseActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val isOffline = !isOnline

         if(UserPreferences.getInstance(applicationContext).userToken.isNotEmpty() || isOffline){
           if( intent?.action   != TASK_EXIT){
               TaskLoader.offlineMode = isOffline
               TaskLoader.setTokenVal(UserPreferences.getInstance(applicationContext).userToken)
               val intent = Intent(this, TasksActivity::class.java)
               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
               startActivity(intent)
           }
            return
        }

        setContentView(R.layout.activity_login)
    }


    companion object {

    }
}
