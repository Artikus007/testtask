package com.online.job.test.helpers

import com.online.job.test.pojo.User

object UserHelper {
     const val INVALID_EMAIL = 1
     const val EMPTY_EMAIL = 2

       fun isEmailValid(email: String): Int {
        return when{
            !email.contains("@")->INVALID_EMAIL
            email.isEmpty() -> EMPTY_EMAIL
            else -> 0
        }
    }

       fun isPasswordValid(password: String): Boolean {
        return password.length > 4
    }
}