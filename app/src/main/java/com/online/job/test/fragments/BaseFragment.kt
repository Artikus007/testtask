package com.online.job.test.fragments

import android.support.v4.app.Fragment
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment : Fragment() {

 val disposable  = CompositeDisposable()

 override fun onDestroyView() {
     super.onDestroyView()
     disposable.clear()
 }

}